#-------------------------------------------------
#
# Project created by QtCreator 2013-12-11T23:29:57
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = creator
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    parser.cpp \
    templatereader.cpp

HEADERS += \
    parser.h \
    templatereader.h \
    base.h

OTHER_FILES += \
    concrete.txt \
    sample1.txt
