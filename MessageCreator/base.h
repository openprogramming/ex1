#ifndef BASE_H
#define BASE_H

class IMessage
{
public:
    IMessage(){}

    virtual char* Write() = 0;
    virtual void Read( const char* buffer ) = 0;

};

#endif // BASE_H
