#include "parser.h"

#include <cctype>
#include <sstream>

#include "templatereader.h"

using namespace std;

Parser::Parser( string name )
{
    TemplateReader templates("concrete.txt");
    ifstream file(name.c_str(), ifstream::in );
    string param;
    while( getline(file, param, '|') )
    {
        strings.push_back(param);
    }

    vector<string>:: iterator it, endIt = strings.end();
    for(it = strings.begin(); it!=endIt; it++)
    {
        size_t idx=  (*it).find_first_of(':');
        string s1 = (*it).substr(0, idx);
        string s2 = (*it).substr(idx+1, (*it).size()-idx-1);
        names.push_back(s1);
        types.push_back(s2);

        for(size_t i=0;i<s2.size();++i)
        {
            s2[i] = toupper(s2[i]);
        }

        stringstream addition1;
        stringstream addition2;
        stringstream addition3;
        stringstream addition4;

        if(s2=="INT")
        {
            addition1 << "\tint m_" << s1 << ";";
            templates.addMembers(addition1.str());

            addition2 << "void set" << s1 << "( int " << s1 << " ){" \
                      << " m_" << s1 << " = " << s1 << ";" \
                      << "}\n" \
                      << "int get" << s1 << "() const {" \
                      << "return m_" << s1 << ";}\n";
            templates.AddGetSet(addition2.str());

            addition3 << "\t\tbuffer << m_" << s1 << " << \" \";";
            templates.AppendWrite( addition3.str() );

            addition4 << "\t\tlocalbuffer >> m_" << s1 << ";";
            templates.AppendRead(addition4.str());

        }
        else if(s2=="BOOL")
        {
            addition1 << "\tbool m_" << s1 << ";";
            templates.addMembers(addition1.str());

            addition2 << "void set" << s1 << "( bool " << s1 << " ){" \
                      << " m_" << s1 << " = " << s1 << ";" \
                      << "}\n" \
                      << "bool get" << s1 << "() const {" \
                      << "return m_" << s1 << ";}\n";
            templates.AddGetSet(addition2.str());

            addition3 << "\t\tbuffer << m_" << s1 << " << \" \";";
            templates.AppendWrite( addition3.str() );

            addition4 << "\t\tlocalbuffer >> m_" << s1 << ";";
            templates.AppendRead(addition4.str());
        }
    }

    templates.Generate();
}

void Parser::Print()
{
//    vector<string>:: iterator it, endIt = strings.end();
//    for(it = strings.begin(); it!=endIt; it++)
//        printf("%s\n", (*it).c_str());
}
