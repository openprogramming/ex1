#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <fstream>
#include <vector>

class Parser
{
private:
    std::vector<std::string> strings;
    std::vector<std::string> types;
    std::vector<std::string> names;

public:
    Parser( std::string name );

    void Print();
};

#endif // PARSER_H
