#include "templatereader.h"

#include <fstream>
#include <sstream>

using namespace std;

TemplateReader::TemplateReader(string f2 )
{
    ifstream tf2(f2);
    template2 = string((istreambuf_iterator<char>(tf2)),
                     istreambuf_iterator<char>());



}


void TemplateReader::AddGetSet(string gs)
{
    size_t p = template2.find("/*get-set*/");
    stringstream newStr;
    newStr << template2.substr(0, p) << "\n";
    newStr << gs << "\n";
    newStr << template2.substr(p, template2.size()-p);

    template2 = newStr.str();
}

void TemplateReader::AppendRead(string r)
{
    size_t p = template2.find("/*read*/");
    stringstream newStr;
    newStr << template2.substr(0, p) << "\n";
    newStr << r << "\n";
    newStr << template2.substr(p, template2.size()-p);

    template2 = newStr.str();
}

void TemplateReader::AppendWrite(string w)
{
    size_t p = template2.find("/*write*/");
    stringstream newStr;
    newStr << template2.substr(0, p) << "\n";
    newStr << w << "\n";
    newStr << template2.substr(p, template2.size()-p);

    template2 = newStr.str();
}
void TemplateReader::addMembers(string m)
{
    printf("Adding field: %s\n", m.c_str());

    size_t p = template2.find("/*members*/");
    stringstream newStr;
    newStr << template2.substr(0, p) << "\n";
    newStr << m << "\n";
    newStr << template2.substr(p, template2.size()-p);

    template2 = newStr.str();
}

void TemplateReader::Generate()
{
    //printf(template2.c_str());

    ofstream generated("out.h");
    generated << template2;
}
