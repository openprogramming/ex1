#ifndef TEMPLATEREADER_H
#define TEMPLATEREADER_H

#include <string>

class TemplateReader
{
public:
    TemplateReader( std::string f2 );

    void addMembers(std::string m);

private:
    std::string template1;
    std::string template2;

public:
    void Generate();
    void AddGetSet(std::string gs);
    void AppendWrite(std::string w);
    void AppendRead(std::string r);
};

#endif // TEMPLATEREADER_H
