#-------------------------------------------------
#
# Project created by QtCreator 2013-12-14T23:59:07
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = TestGenerated
CONFIG   += console
CONFIG   -= app_bundle

QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app


SOURCES += main.cpp

OTHER_FILES +=

HEADERS += \
    ../MessageCreator/base.h \
    ../MessageCreator/out.h
