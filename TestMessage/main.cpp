#include "../MessageCreator/base.h"
#include "../MessageCreator/out.h"

#define UNUSED(x) (void)(x)

int main(int argc, char *argv[])
{
    UNUSED(argc); UNUSED(argv);

    ConcreteMessage cm;
    cm.setfiled1(5);
    cm.setfield2(true);

    char* test = cm.Write();

    ConcreteMessage cm2;
    cm2.Read( test );

    char* test2 = cm2.Write();
    printf(test2);

    return 0;
}
